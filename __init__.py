from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql:///HRMS'
app.config['SECRET_KEY']="HRMS"
db = SQLAlchemy(app)

from .dbmodels import User
db.create_all()
db.session.commit()

from .auth import login, register, logout
app.add_url_rule('/','',login, methods=['GET','POST'])
app.add_url_rule('/login','login',login, methods=['GET','POST'])
app.add_url_rule('/register','register',register, methods=['GET','POST'])
app.add_url_rule('/logout','logout',logout)

from .dashboard import dashboard
app.add_url_rule('/dashboard','dashboard',dashboard)

if __name__ ==  "__main__":
    app.run(debug=True)

