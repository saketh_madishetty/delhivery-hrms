from .import app,db
from flask import flash, g, redirect, render_template, request, session, url_for
import functools
from werkzeug.security import generate_password_hash, check_password_hash
from .dbmodels import User

@app.route('/', methods=['GET','POST'])
@app.route('/login', methods=['GET','POST'])
def login():
    if(request.method == "POST"):
        username = request.form['username']
        password = request.form['password']
        dbuser = User.query.filter_by(username=username).first()

        if dbuser is None:
            flash("Incorrect Username or Password")
        elif check_password_hash(dbuser.password,password):
            session['user_id'] = dbuser.id        
            flash(f"You're logged in as {username}")
            return redirect(url_for('dashboard'))
        else:
            flash("Password Incorrect")   

    elif (request.method == "GET"):
        if g.user is not None:
            return redirect(url_for('dashboard'))

    return render_template('auth/login.html')

@app.route('/register',methods=['GET','POST'])
def register():
    if(request.method == "POST"):
        username = request.form['username']
        password = request.form['password']
        confirm_password = request.form['confirm_password']
        dbuser = User.query.filter_by(username=username).first()

        if dbuser is not None:
            flash('Username already exists.')
        elif password == confirm_password:
            user = User(username=username,password=generate_password_hash(password))
            db.session.add(user)
            db.session.commit()
            flash("Successfully Registered!")
            return redirect(url_for("login"))
        else:
            flash("Password did not match.")  

    elif (request.method == "GET"):
        if g.user is not None:
            return redirect(url_for('dashboard'))  

    return render_template('auth/register.html')

@app.route('/logout')
def logout():
    session.clear()
    return redirect(url_for('login'))

@app.before_request
def load_logged_in_user():
    user_id = session.get('user_id')

    if user_id is None:
        g.user = None
    else:
        g.user = User.query.filter_by(id=user_id).first()

def login_required(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if g.user is None:
            flash("You are not logged in. Please login first!")
            return redirect(url_for('login'))

        return view(**kwargs)

    return wrapped_view
