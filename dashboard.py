from . import app, db
from flask import flash, Blueprint, g,  redirect, render_template, request, session, url_for
from .dbmodels import User
from .auth import login_required

@app.route('/dashboard')
@login_required
def dashboard():
        return render_template('dashboard/dashboard.html')    